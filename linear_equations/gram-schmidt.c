#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

#define RND (double)rand()/RAND_MAX


void GS_QR_solve(gsl_matrix *Q, gsl_matrix *R, gsl_vector *b, gsl_vector *x) {
	int m = R->size1;
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
	for(int i=m-1; i>=0; i--) {
		double s = 0;
		for(int k=i+1; k<m; k++) {
			s += gsl_matrix_get(R, i, k) * gsl_vector_get(x, k);
		}
		gsl_vector_set(x, i, (gsl_vector_get(x, i)-s)/gsl_matrix_get(R, i, i));
	}
}

void GS_QR_decomp(gsl_matrix *A, gsl_matrix *R) {
	int m = A->size2;
	for(int i=0; i<m; i++) {
		gsl_vector_view Ai = gsl_matrix_column(A, i);
		double norm = gsl_blas_dnrm2(&Ai.vector);
		gsl_vector_scale(&Ai.vector, 1.0/norm);
		gsl_matrix_set(R, i, i, norm);

		for(int j=i+1; j<m; j++) {
			gsl_vector_view Aj = gsl_matrix_column(A, j);
			double dotprod = 0;
			gsl_blas_ddot(&Ai.vector, &Aj.vector, &dotprod);
			gsl_blas_daxpy(-dotprod, &Ai.vector, &Aj.vector);
			gsl_matrix_set(R, i, j, dotprod);
		}
	}
}

void inverse(gsl_matrix *Q, gsl_matrix *R, gsl_matrix *B) {
	int n = R->size1;
	gsl_vector *x = gsl_vector_calloc(n);
	gsl_vector *b = gsl_vector_calloc(n);

	for(int i=0; i<n; i++) {
		gsl_vector_set(b, i, 1.0);
		GS_QR_solve(Q, R, b, x);
		gsl_vector_set(b, i, 0.0);
		gsl_matrix_set_col(B, i, x);
	}
}

void print_matrix(gsl_matrix *M) {
	for(int row=0; row<M->size1; row++){
		for(int col=0; col<M->size2; col++) {
			printf("%.2f ", gsl_matrix_get(M, row, col));
		}
		printf("\n");
	}
}

void print_vector(gsl_vector *V) {
	for(int row=0; row<V->size; row++) {
		printf("%.2f\n", gsl_vector_get(V, row));
	}
}

int main () {
	int Arow = 7, Acol = 5;

	//Test the QR decomposition
	printf("Test the QR decomposition\n");
	gsl_matrix *R = gsl_matrix_alloc(Acol, Acol);
	gsl_matrix *A = gsl_matrix_alloc(Arow, Acol);
	gsl_matrix *QTQ = gsl_matrix_alloc(Acol, Acol);
	gsl_matrix *QR = gsl_matrix_alloc(Arow, Acol);
	gsl_matrix_set_zero(R);
	for(int i=0; i<Arow; i++) {
		for(int j=0; j<Acol; j++) {
			gsl_matrix_set(A, i, j, RND);
		}
	}

	printf("Matrix A\n");
	print_matrix(A);
	printf("\n");

	GS_QR_decomp(A, R);
	printf("Matrix Q\n");
	print_matrix(A);
	printf("\nTest that R is upper triangular:\n");
	printf("Matrix R\n");
	print_matrix(R);
	printf("\n");
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, A, A, 0.0, QTQ);
	printf("Test that Q^TQ=I:\n");
	printf("Matrix Q^TQ\n");
	print_matrix(QTQ);

	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, R, 0.0, QR);
	printf("\nTest that QR=A:\n");
	printf("Matrix QR\n");
	print_matrix(QR);
	printf("\n\n");

	//Test the backsubstitution
	printf("Test the backsubstitution\n");
	gsl_vector *x = gsl_vector_alloc(Acol);
	gsl_vector *b = gsl_vector_alloc(Acol);
	gsl_matrix *RR = gsl_matrix_alloc(Acol, Acol);
	gsl_matrix *AA = gsl_matrix_alloc(Acol, Acol);
	gsl_matrix *QQ = gsl_matrix_alloc(Acol, Acol);
	for(int i=0; i<Acol; i++) {
		for(int j=0; j<Acol; j++) {
			gsl_matrix_set(AA, i, j, RND);
		}
	}
	for(int i=0; i<b->size; i++) {
		gsl_vector_set(b, i, RND);
	}
	gsl_matrix_memcpy(QQ, AA);

	printf("Matrix A\n");
	print_matrix(AA);
	GS_QR_decomp(QQ, RR);
	printf("\nMatrix R\n");
	print_matrix(RR);
	printf("\nVector b\n");
	print_vector(b);
	GS_QR_solve(QQ, RR, b, x);
	printf("\nVector x\n");
	print_vector(x);

	gsl_blas_dgemv(CblasNoTrans, 1.0, AA, x, 0.0, b);
	printf("\nTest that Ax=b:\n");
	printf("Vector Ax\n");
	print_vector(b);
	printf("\n\n");

	//Test the inverse
	printf("\nTest the inverse matrix\n");
	gsl_matrix *RRR = gsl_matrix_alloc(Acol, Acol);
	gsl_matrix *AAA = gsl_matrix_alloc(Acol, Acol);
	gsl_matrix *BBB = gsl_matrix_alloc(Acol, Acol);
	gsl_matrix *CCC = gsl_matrix_alloc(Acol, Acol);
	gsl_matrix *QQQ = gsl_matrix_alloc(Acol, Acol);
	for(int i=0; i<Acol; i++) {
		for(int j=0; j<Acol; j++) {
			gsl_matrix_set(AAA, i, j, RND);
		}
	}
	gsl_matrix_memcpy(QQQ, AAA);	

	printf("Matrix A\n");
	print_matrix(AAA);

	GS_QR_decomp(QQQ, RRR);
	inverse(QQQ, RRR, BBB);
	printf("\nMatrix A⁻¹\n");
	print_matrix(BBB);

	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, R, 0.0, QR);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, AAA, BBB, 0.0, CCC);
	printf("\nTest that AA⁻¹=I:\n");
	printf("Matrix AA⁻¹\n");
	print_matrix(CCC);

	gsl_matrix_free(R);
	gsl_matrix_free(RR);
	gsl_matrix_free(RRR);
	gsl_matrix_free(A);
	gsl_matrix_free(AA);
	gsl_matrix_free(AAA);
	gsl_matrix_free(BBB);
	gsl_matrix_free(CCC);
	gsl_matrix_free(QQ);
	gsl_matrix_free(QQQ);
	gsl_matrix_free(QTQ);
	gsl_vector_free(b);
	gsl_vector_free(x);
	return 0;
}
