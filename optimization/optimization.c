#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>


void givens_QR_decomp(gsl_matrix* A) {
	for(int q=0; q<A->size2; q++) {
		for(int p=q+1; p<A->size1; p++) {
			double theta = atan2(gsl_matrix_get(A,p,q), gsl_matrix_get(A,q,q));
			for(int k=q; k<A->size2; k++) {
				double xq = gsl_matrix_get(A, q, k);
				double xp = gsl_matrix_get(A, p, k);
				gsl_matrix_set(A, q, k, xq*cos(theta) + xp*sin(theta));
				gsl_matrix_set(A, p, k, -xq*sin(theta) + xp*cos(theta));
			}
			gsl_matrix_set(A, p, q, theta);
		}
	}
}

void givens_QR_QTvec(gsl_matrix *QR, gsl_vector *v) {
	for(int q=0; q<QR->size2; q++) {
		for(int p=q+1; p<QR->size1; p++) {
			double theta = gsl_matrix_get(QR, p, q);
			double vq = gsl_vector_get(v, q);
			double vp = gsl_vector_get(v, p);
			gsl_vector_set(v, q, vq*cos(theta) + vp*sin(theta));
			gsl_vector_set(v, p, -vq*sin(theta) + vp*cos(theta));
		}
	}
}

void givens_QR_solve(gsl_matrix *QR, gsl_vector *b, gsl_vector *x) {
	givens_QR_QTvec(QR, b);

	for(int i=QR->size2-1; i>=0; i--) {
		double s = 0;
		for(int k=i+1; k<QR->size2; k++) {
			s += gsl_matrix_get(QR, i, k)*gsl_vector_get(x, k);
		}
		gsl_vector_set(x, i, (gsl_vector_get(b, i)-s)/gsl_matrix_get(QR, i, i));
	}
}


int newton (double f(gsl_vector *x), void gradient(gsl_vector *x, gsl_vector *df), void hessian(gsl_vector *x, gsl_matrix *H), gsl_vector *xstart, double eps) {
	int n = xstart->size;

	gsl_vector *dfx = gsl_vector_alloc(n);
	gsl_vector *HDx = gsl_vector_alloc(n);
	gsl_vector *Dx = gsl_vector_alloc(n);
	gsl_vector *z = gsl_vector_alloc(n);
	gsl_vector *dfz = gsl_vector_alloc(n);
	gsl_matrix *H = gsl_matrix_alloc(n,n);

	double dx = 0.001, fx, fz, Dxdfx;
	int ncalls = 0;

	do{
		ncalls++;
		fx = f(xstart);
		gradient(xstart,dfx);
		hessian(xstart,H);

		givens_QR_decomp(H);
		gsl_vector_scale(dfx,-1);
		givens_QR_solve(H,dfx,Dx);
		gsl_vector_scale(dfx,-1);
		gsl_blas_dgemv(CblasNoTrans, 1, H, Dx, 0.0, HDx);

		double s = 1.0, alpha = 1e-3;
		do{ 
			s/=2;
			gsl_vector_memcpy(z,Dx);
			gsl_vector_scale(z,s);
			gsl_vector_add(z,xstart);
			fz = f(z);
			gsl_blas_ddot(Dx,dfx,&Dxdfx);
		}while (fabs(fz)>fabs(fx) + alpha*s*Dxdfx && s>0.0002);
		gsl_vector_memcpy(xstart,z);
	}while (gsl_blas_dnrm2(Dx)>dx && gsl_blas_dnrm2(dfx)>eps);


	gsl_vector_free(dfx);
	gsl_vector_free(HDx);
	gsl_vector_free(Dx);
	gsl_vector_free(z);
	gsl_vector_free(dfz);
	gsl_matrix_free(H);
	return ncalls;
}

void gradient_numeric (double f(gsl_vector *x), gsl_vector *x, gsl_vector *dfx, double dx) {
	int n = x->size;

	double fx = f(x);
	for (int i=0; i<n; i++) {
		gsl_vector_set(x,i,gsl_vector_get(x,i)+dx);
		gsl_vector_set(dfx,i,(f(x)-fx)/dx);
		gsl_vector_set(x,i,gsl_vector_get(x,i)-dx);
	}
}

int newton_broyden (double f(gsl_vector *x), gsl_vector *x, double dx, double eps) {
	int n = x->size;

	gsl_vector *y = gsl_vector_alloc(n);
	gsl_vector *z = gsl_vector_alloc(n);
	gsl_vector *s = gsl_vector_alloc(n);
	gsl_vector *Dx = gsl_vector_alloc(n);
	gsl_vector *dfx = gsl_vector_alloc(n);
	gsl_vector *dfz = gsl_vector_alloc(n);
	gsl_vector *Hinvy = gsl_vector_alloc(n);
	gsl_vector *u = gsl_vector_alloc(n);
	gsl_vector *v = gsl_vector_alloc(n);
	gsl_matrix *Hinv = gsl_matrix_alloc(n,n);
	gsl_matrix *A = gsl_matrix_alloc(n,n);
	gsl_matrix *dH = gsl_matrix_alloc(n,n);

	double fx, fz, dotproduct;
	int ncalls = 0;

	gradient_numeric(f,x,dfx,dx);
	gsl_matrix_set_identity(Hinv);
	fx = f(x);

	do{
		ncalls++;
		gsl_blas_dgemv(CblasNoTrans,1,Hinv,dfx,0,Dx);
		gsl_vector_scale(Dx,-1);
		gsl_vector_memcpy(s,Dx);
		gsl_vector_scale(s,2);
		double alpha = 0.1;

		do{
			gsl_vector_scale(s,0.5);
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,s);
			fz = f(z);
			gsl_blas_ddot(s,dfx,&dotproduct);

			if (fabs(fz)<fabs(fx) + alpha*dotproduct) {
				break;
			}
			if (gsl_blas_dnrm2(s)<dx) {
				gsl_matrix_set_identity(Hinv);
				break;
			}
		}while (fabs(fz)>fabs(fx) + alpha*dotproduct);
		gradient_numeric(f,z,dfz,dx);
		gsl_vector_memcpy(y,dfz);
		gsl_vector_sub(y,dfx);
		gsl_blas_dgemv(CblasNoTrans,1,Hinv,y,0,Hinvy);

		gsl_vector_memcpy(u,s);
		gsl_vector_sub(u,Hinvy);
		for (int i=0; i<n; i++) {
			for (int j=0; j<n; j++) {
				gsl_matrix_set(A,i,j,gsl_vector_get(u,i)*gsl_vector_get(s,j));
			}
		}
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,A,Hinv,0,dH);
		gsl_blas_dgemv(CblasNoTrans,1,Hinv,s,0,v);
		gsl_blas_ddot(y,v,&dotproduct);
		gsl_matrix_scale(dH,1/dotproduct);
		gsl_matrix_add(Hinv,dH);

		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(dfx,dfz);
		fx = fz;
	}while (gsl_blas_dnrm2(Dx)>dx && gsl_blas_dnrm2(dfx)>eps);


	gsl_vector_free(y);
	gsl_vector_free(z);
	gsl_vector_free(s);
	gsl_vector_free(Dx);
	gsl_vector_free(dfx);
	gsl_vector_free(dfz);
	gsl_vector_free(u);
	gsl_vector_free(v);
	gsl_matrix_free(Hinv);
	gsl_matrix_free(A);
	gsl_matrix_free(dH);
	return ncalls;
}

double rosenbrock (gsl_vector *vec) {
	double x = gsl_vector_get(vec,0);
	double y = gsl_vector_get(vec,1);
	double func = pow((1-x),2) + 100*pow((y-x*x),2);
	
	return func;
}

double himmelblau (gsl_vector *vec) {
	double x = gsl_vector_get(vec,0);
	double y = gsl_vector_get(vec,1);
	double func = pow((y+x*x-11),2) + pow((x+y*y-7),2);

	return func;
}

void rosenbrock_g (gsl_vector *vec, gsl_vector *fx) {
	double x = gsl_vector_get(vec,0);
	double y = gsl_vector_get(vec,1);
	gsl_vector_set(fx,0,400*pow(x,3) - 400*x*(y-0.005)-2);
	gsl_vector_set(fx,1,200*y - 200*pow(x,2));
}

void himmelblau_g (gsl_vector *vec, gsl_vector *fx) {
	double x = gsl_vector_get(vec,0);
	double y = gsl_vector_get(vec,1);
	gsl_vector_set(fx,0,4*pow(x,3) + 4*x*(y-10.5) + 2*(pow(y,2)-7));
	gsl_vector_set(fx,1,4*pow(y,3) + 4*y*(x-6.5) + 2*pow(x,2) - 22);
}

void rosenbrock_H (gsl_vector *vec, gsl_matrix *H) {
	double x = gsl_vector_get(vec,0);
	double y = gsl_vector_get(vec,1);
	double H00, H01, H10, H11;
	H00 = 400*3*pow(x,2) - 400*(y-0.005);
	H01 = -400*x;
	H10 = -200*2*x;
	H11 = 200;
	gsl_matrix_set(H,0,0,H00);
	gsl_matrix_set(H,0,1,H01);
	gsl_matrix_set(H,1,0,H10);
	gsl_matrix_set(H,1,1,H11);
}

void himmelblau_H (gsl_vector *vec, gsl_matrix *H) {
	double x = gsl_vector_get(vec,0);
	double y = gsl_vector_get(vec,1);
	double H00, H01, H10, H11;
	H00 = 12*pow(x,2) + 4*(y-10.5);
	H01 = 4*y + 4*x;
	H10 = 4*y + 4*x;
	H11 = 12*pow(y,2) + 4*(x-6.5);
	gsl_matrix_set(H,0,0,H00);
	gsl_matrix_set(H,0,1,H01);
	gsl_matrix_set(H,1,0,H10);
	gsl_matrix_set(H,1,1,H11);
}

void print_vector(gsl_vector *V) {
	for(int row=0; row<V->size; row++) {
		printf("%.2f\n", gsl_vector_get(V, row));
	}
}


int main () {
	int dim = 2;
	int calls = 0;
	int x1 = 4, x2 = -2;
	double eps = 1e-3, dx = 1e-6;
	gsl_vector *xstart = gsl_vector_alloc(dim);
	gsl_vector_set(xstart,0,x1);
	gsl_vector_set(xstart,1,x2);


	printf("Epilon = 10⁻³\ndx = 10⁻⁶\nInital value of xstart:\n");
	print_vector(xstart);
	//PART A
	printf("\nPART A - Normal Newton\n");

	//Rosenbrock
	calls = newton(rosenbrock, rosenbrock_g, rosenbrock_H, xstart, eps);
	printf("\n\nMinimum of Rosenbrock:\n");
	print_vector(xstart);
	printf("Iterations required: %d", calls);

	//Himmelblau
	gsl_vector_set(xstart,0,x1);
	gsl_vector_set(xstart,1,x2);
	calls = newton(himmelblau, himmelblau_g, himmelblau_H, xstart, eps);
	printf("\n\nMinimum of Himmelblau:\n");
	print_vector(xstart);
	printf("Iterations required: %d", calls);

	//PART B
	printf("\n\n\nPART B - Quasi-Newton with Broyden correction\n");

	//Rosenbrock
	gsl_vector_set(xstart,0,x1);
	gsl_vector_set(xstart,1,x2);
	calls = newton_broyden(rosenbrock, xstart, dx, eps);
	printf("\n\nMinimum of Rosenbrock:\n");
	print_vector(xstart);
	printf("Iterations required: %d", calls);
	
	//Himmelblau
	gsl_vector_set(xstart,0,x1);
	gsl_vector_set(xstart,1,x2);
	calls = newton_broyden(himmelblau, xstart, dx, eps);
	printf("\n\nMinimum of Himmelblau:\n");
	print_vector(xstart);
	printf("Iterations required: %d", calls);
	
	gsl_vector_free(xstart);
	return 0;
}
