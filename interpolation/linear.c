#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>

#define RND (double)rand()/RAND_MAX
typedef struct {int n; double *x, *y, *b, *c;} qspline;


qspline* qspline_alloc(int n, gsl_vector *x, gsl_vector *y) {
	qspline *S = (qspline*) malloc(sizeof(qspline));
	S->b = (double*) malloc((n-1)*sizeof(double));
	S->c = (double*) malloc((n-1)*sizeof(double));
	S->x = (double*) malloc(n*sizeof(double));
	S->y = (double*) malloc(n*sizeof(double));

	for(int i=0; i<n; i++) {
		S->x[i] = gsl_vector_get(x,i);
		S->y[i] = gsl_vector_get(y,i);
	}
	S->n = n;

	double p[n-1], h[n-1];

	for(int i=0; i<n-1; i++) {
		h[i] = gsl_vector_get(x,i+1) - gsl_vector_get(x,i);
		p[i] = (gsl_vector_get(y,i+1) - gsl_vector_get(y,i))/h[i];
	}

	S->c[0] = 0;
	for(int i=0; i<n-2; i++) {
		S->c[i+1] = (p[i+1] - p[i] - S->c[i]*h[i])/h[i+1];
	}

	S->c[n-2]/=2;
	for(int i=n-3; i>=0; i--) {
		S->c[i] = (p[i+1] - p[i] - S->c[i+1]*h[i+1])/h[i];
	}

	for(int i=0; i<n-1; i++) {
		S->b[i] = p[i] - S->c[i]*h[i];
	}

	return S;
}

void qspline_free(qspline *S) {
	free(S->x);
	free(S->y);
	free(S->b);
	free(S->c);
	free(S);
}

double qspline_eval(qspline *S, double z) {
	double *x = S->x;
	//assert(z>=x[0] && z<=x[S->n-1]);
	int i=0, j=S->n-1;

	while(j-i>1) {
		int m = (i+j)/2;
		if(z>x[m]) {
			i = m;
		}else {
			j = m;
		}
	}
	double h = z - x[i];

	return S->y[i] + h*(S->b[i] + h*S->c[i]);
}

double qspline_deriv(qspline *S, double z) {
	double *x = S->x;
	if(z<x[0] || z>x[S->n-1]) {
		printf("qspline_deriv: z=%g out of range", z);
	}
	int i=0, j=S->n-1;

	while(j-i>1) {
		int m = (i+j)/2;
		if(z>x[m]) {
			i = m;
		}else {
			j = m;
		}
	}
	double h = z - x[i];

	return S->b[i] + 2*h*S->c[i];
}

double qspline_integ () {
	
}

double lin_interp(int n, gsl_vector *x, gsl_vector *y, double z) {
	assert(n>1);

	int i=0, j=n-1;
	while(j-i>1) {
		int m = (i+j)/2;
		if(z>gsl_vector_get(x,m)) {
			i = m;
		} else {		
			j = m;
		}
	}
	return gsl_vector_get(y,i) + (gsl_vector_get(y,i+1)-gsl_vector_get(y,i)) / (gsl_vector_get(x,i+1)-gsl_vector_get(x,i)) * (z-gsl_vector_get(x,i));
}

void lin_interp_integ(gsl_vector *x, gsl_vector *y, double z) {
	int i;
	double integ = 0, xi, xiplus1, yi, yiplus1;
	for(i=0; gsl_vector_get(x,i+1)<=z; i++) {
		xi = gsl_vector_get(x,i);
		xiplus1 = gsl_vector_get(x,i+1);
		yi = gsl_vector_get(y,i);
		yiplus1 = gsl_vector_get(y,i+1);

		integ += yi * (xiplus1 - xi) + 1/2 * (yiplus1 - yi)/(xiplus1 - xi) * pow((xiplus1 - xi),2);
		printf("%f\t%f\n", xi, integ);
	}
	xi = gsl_vector_get(x,i);
	xiplus1 = gsl_vector_get(x,i+1);
	yi = gsl_vector_get(y,i);
	yiplus1 = gsl_vector_get(y,i+1);
	integ += yi * (z - xi) + 1/2 * (yiplus1 - yi)/(xiplus1 - xi) * pow((z - xi),2);
	printf("%f\t%f\n", gsl_vector_get(x,i), integ);
}

int main() {
	//PART A
	double i, step = 0.2;
	int index = 0;
	int dim = 33;	
																																																																																								
	gsl_vector *x = gsl_vector_alloc(dim);	
	gsl_vector *y = gsl_vector_alloc(dim);

	for(i=0; i<dim/(1/step); i+=step) {
		double j = i;
		gsl_vector_set(x,index,j);
		index++;
	}
	
	index = 0;
	for(i=0; i<dim/(1/step); i+=step) {
		double j = sin(i);
		gsl_vector_set(y,index,j);
		index++;
	}

	for(i=0; i<x->size; i++) {
		printf("%f\t%f\n", gsl_vector_get(x,i), gsl_vector_get(y,i));
	}

	printf("\n\n");
	index = 0;
	for(i=0; i<dim/(1/step); i+=step) {
		double result = lin_interp(dim, x, y, i);
		printf("%f\t%f\n", gsl_vector_get(x,index), result);
		index++;
	}

	printf("\n\n");
	lin_interp_integ(x, y, gsl_vector_get(x,x->size-2));
	
	gsl_vector_free(x);
	gsl_vector_free(y);
	
	//PART B
	int n = 20; 

	gsl_vector *xx = gsl_vector_alloc(n);
	gsl_vector *yy = gsl_vector_alloc(n);

	printf("\n\n");
	for (int i=0; i<n; i++) {
		gsl_vector_set(xx,i,i);
		gsl_vector_set(yy,i,RND);
		printf("%g\t%g\n", gsl_vector_get(xx,i), gsl_vector_get(yy,i));
	}

	qspline *qs = qspline_alloc(n,xx,yy);

	printf("\n\n");
	for (double i=0; i<=n-1; i+=0.05) {
		printf("%g\t%g\n", i, qspline_eval(qs,i));
	}

	printf("\n\n");
	for (double i=0; i<=n-1; i+=0.05) {
		printf("%g\t%g\n", i, qspline_deriv(qs,i));
	}
	


	qspline_free(qs);
	return 0;
}
