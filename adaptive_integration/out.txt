PART A

f(x) = sqrt(x)
integrated from 0 to 1
Q = 0.663017
Calls = 468

f(x) = 4*sqrt(1-(1-x)²)
integrated from 0 to 1
Q = 3.13651
Calls = 1248


PART B

f(x) = 4*sqrt(1-(1-x)²)
Intgrated for -inf to inf
Segmentation fault during recursion