#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>


double integ (double f(double), double a, double b, double acc, double eps, double f2, double f3, int nrec) {
	assert(nrec<1000000);

	double f1 = f ((a+b)/2);
	double f4 = f (a+5*(b-a)/6);
	double Q = (2*f1+f2+f3+2*f4)/6*(b-a);
	double q = (f1+f4+f2+f3)/4*(b-a);
	double tol = acc + eps*fabs(Q);
	double err = fabs(Q-q);

	if (err<tol) {
		return Q;
	}else {
		double Q1 = integ (f,a,(a+b)/2,acc/sqrt(2.),eps,f1,f2,nrec++);
		double Q2 = integ (f,(a+b)/2,b,acc/sqrt(2.),eps,f3,f4,nrec++);
		return Q1+Q2;
	}
}

double adapt (double f(double), double a, double b, double acc, double eps) {
	double f2 = f (a+2*(b-a)/6);
	double f3 = f (a+4*(b-a)/6);
	int nrec = 0;
	
	return integ(f,a,b,acc,eps,f2,f3,nrec);
}

double integ_inf (double f(double), double a, double b, double f2, double f3, double acc, double eps, int nrec) {
	assert(nrec<1000000);

	double f1 = f (a+(b-a)/6);
	double f4 = f (a+5*(b-a)/6);
	double Q = (2*f1+f2+f3+2*f4)*(b-a)/6;
	double q = (f1+f2+f3+f4)*(b-a)/4;
	double tol = acc + eps*fabs(Q);
	double err = fabs(Q-q);

	if (err<tol) {
		return Q;
	}else {
		double Q1 = integ_inf (f,a,(a+b)/2,f1,f2,acc/sqrt(2.0),eps,nrec++);
		double Q2 = integ_inf (f,(a+b)/2,b,f2,f3,acc/sqrt(2.0),eps,nrec++);
		return Q1+Q2;
	}
}

double adapt_inf (double f(double), double a, double b, double acc, double eps) {
	int nrec = 0;

	if (isinf(a) && isinf(b)) {
		double ft(double t) {
			return f(t/1-pow(t,2))*(1+pow(t,2))/pow(1-t*t,2);
		}
		double a = -1;
		double b = 1;
		double f2 = ft (a+(b-a)/3);
		double f3 = ft (a+2*(b-a)/3);

		return integ_inf(ft,a,b,f2,f3,acc,eps,nrec);
	}else if (isinf(b)) {
		double ft(double t) {
			return f(a*t/(1-t))*1/pow(1-t,2);
		}

		double a = 0;
		double b = 1;
		double f2 = ft (a+(b-a)/3);
		double f3 = ft (a+2*(b-a)/3);

		return integ_inf(ft,a,b,f2,f3,acc,eps,nrec);
	}else if (isinf(a)) {
		double ft(double t) {
			return f(b+t/(1+t))*1/pow(1+t,2);
		}

		double a = -1;
		double b = 0;
		double f2 = ft (a+(b-a)/3);
		double f3 = ft (a+2*(b-a)/3);

		return integ_inf(ft,a,b,f2,f3,acc,eps,nrec);
	}else {
		double f2 = f (a+(b-a)/3);
		double f3 = f (a+2*(b-a)/3);

		return integ_inf(f,a,b,f2,f3,acc,eps,nrec);
	}
}

int main () {
	int calls = 0;
	double a = 0, b = 1;
	double acc = 1e-3, eps = 1e-3;

	double test_function (double x) {
		calls++;
		return sqrt(x);
	}

	double calc_function (double x) {
		calls++;
		return 4*sqrt(1-pow(1-x,2));
	}

	double inf_function (double x) {
		calls++;
		return 1.0/(1+x*x);
	}

	printf("PART A\n\n");
	double Q1 = adapt_inf (test_function,a,b,acc,eps);
	printf("f(x) = sqrt(x)\n");
	printf("integrated from 0 to 1\n");
	printf("Q = %g\nCalls = %d\n", Q1, calls);

	calls = 0;
	double Q2 = adapt_inf (calc_function,a,b,acc,eps);
	printf("\nf(x) = 4*sqrt(1-(1-x)²)\n");
	printf("integrated from 0 to 1\n");
	printf("Q = %g\nCalls = %d\n", Q2, calls);

	printf("\n\nPART B\n\n");
	a = -INFINITY;
	b = INFINITY;

	calls = 0;
	//double Q4 = adapt_inf (inf_function,a,b,acc,eps,&err);
	printf("f(x) = 4*sqrt(1-(1-x)²)\n");
	printf("Intgrated for -inf to inf\n");
	printf("Segmentation fault during recursion");
	//printf("Q = %g\nCalls = %d\n", Q4, calls);
	

	return 0;
}
