#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>

void rkstep12 (double t, double h, gsl_vector *y, void f(double t, gsl_vector *y, gsl_vector *dydt), gsl_vector *yh, gsl_vector *err) {
	int n = y->size;
	
	gsl_vector *k0 = gsl_vector_alloc(n);
	gsl_vector *k12 = gsl_vector_alloc(n);
	gsl_vector *yt = gsl_vector_alloc(n);

	f(t,y,k0);
	for (int i=0; i<n; i++) {
		gsl_vector_set(yt,i,gsl_vector_get(y,i) + h/2*gsl_vector_get(k0,i));
	}
	f(t+h/2,yt,k12);
	for (int i=0; i<n; i++) {
		gsl_vector_set(yh,i,gsl_vector_get(y,i) + h*gsl_vector_get(k12,i));
		gsl_vector_set(err,i,(gsl_vector_get(k0,i) - gsl_vector_get(k12,i))*h/2);
	}

	gsl_vector_free(k0);
	gsl_vector_free(k12);
	gsl_vector_free(yt);
}

void driver (double t, double b, double h, gsl_vector *y, double acc, double eps, void stepper(double t, double h, gsl_vector *y, void f(double t, gsl_vector *y, gsl_vector *dydt), gsl_vector *yh, gsl_vector *err), void f(double t, gsl_vector *y, gsl_vector *dydt)) {
	int n = y->size;
	double a = t;

	gsl_vector *yh = gsl_vector_alloc(n);
	gsl_vector *dy = gsl_vector_alloc(n);

	while(t<b) {
		if (t+h>b) {
			h = b-t;
		}
		stepper(t,h,y,f,yh,dy);
		double err = gsl_blas_dnrm2(dy);
		double normy = gsl_blas_dnrm2(yh);
		double tol = (normy*eps+acc)*sqrt(h/(b-a));
		if (err<tol) {
			t += h;
			for (int i=0; i<n; i++) {
				gsl_vector_set(y,i,gsl_vector_get(yh,i));
			}
		}
		if (err>0) {
			h *= pow(tol/err,0.25)*0.95;
		}else {
			h *= 2;
		}
	}

	gsl_vector_free(yh);
	gsl_vector_free(dy);
}

int driver_path (gsl_vector *tpath, gsl_matrix *ypath, double b, double h, gsl_vector *y, double acc, double eps, int max, void stepper(double t, double h, gsl_vector *y, void f(double t, gsl_vector *y, gsl_vector *dydt), gsl_vector *yh, gsl_vector *err), void f(double t, gsl_vector *y, gsl_vector *dydt)) {
	int n = y->size;
	int k = 0;
	double a = gsl_vector_get(tpath,0);
	
	gsl_vector *yh = gsl_vector_alloc(n);
	gsl_vector *dy = gsl_vector_alloc(n);

	while(gsl_vector_get(tpath,k)<b) {
		double t = gsl_vector_get(tpath,k);
		if (t+h>b) {
			h = b-t;
		}
		stepper(t,h,y,f,yh,dy);
		double err = gsl_blas_dnrm2(dy);
		double normy = gsl_blas_dnrm2(yh);
		double tol = (normy*eps + acc)*sqrt(h/(b-a));
		if (err<tol) {
			k++;
			if (k>max-1) {
				return -k;
			}
			gsl_vector_set(tpath,k,t+h);
			for (int i=0; i<n; i++) {
				gsl_vector_set(y,i,gsl_vector_get(yh,i));
				gsl_matrix_set(ypath,k,i,gsl_vector_get(y,i));
			}
		}
		if (err>0) {
			h *= pow(tol/err,0.25)*0.95;
		}else {
			h *= 2;
		}
	}
	
	gsl_vector_free(yh);
	gsl_vector_free(dy);
	return k;
}

void sine (double t, gsl_vector *y, gsl_vector *dydt) {
	gsl_vector_set(dydt,0,gsl_vector_get(y,1));
	gsl_vector_set(dydt,1,-1*gsl_vector_get(y,0));
}

void print_vector(gsl_vector *V) {
	for(int row=0; row<V->size; row++) {
		printf("%.2f\n", gsl_vector_get(V, row));
	}
}

int main () {
	int dim = 2, max=1e7;
	double t =0, b = M_PI, h = 01, acc = 1e-6, eps = 1e-3;

	printf("PART A\n\n");
	gsl_vector *y = gsl_vector_alloc(dim);
	gsl_vector_set(y,0,0);
	gsl_vector_set(y,1,1);

	printf("Startpoint\ta = %g\nEndpoint\tb = %g\n", t, b);
	driver(t,b,h,y,acc,eps,rkstep12,sine);
	printf("Function\ty(b) = %g\nDifferential\ty'(b) = %g\n", gsl_vector_get(y,0), gsl_vector_get(y,1));

	printf("\nPART B\n\n");
	gsl_vector *tpath = gsl_vector_alloc(max);
	gsl_matrix *ypath = gsl_matrix_alloc(max,dim);
	gsl_vector_set(y,0,0);
	gsl_vector_set(y,1,1);

	printf("Startpoint\ta = %g\nEndpoint\tb = %g\n", t, b);
	int k = driver_path(tpath,ypath,b,h,y,acc,eps,max,rkstep12,sine);
	if (k<0) {
		printf("Maximum step count reached\n");
	}
	printf("Function\ty(b) = %g\nDifferential\ty'(b) = %g\n", gsl_vector_get(y,0), gsl_vector_get(y,1));

	//printf("\nODE Data\n");
	printf("\n\n");
	for (int i=0; i<k; i++) {
		printf("%g %g %g %g %g\n", gsl_vector_get(tpath,i), gsl_matrix_get(ypath,i,0), gsl_matrix_get(ypath,i,1), sin(gsl_vector_get(tpath,i)), cos(gsl_vector_get(tpath,i)));
	}
	//printf("\n\nTheoretical Data\n");
	//printf("\n\n");
	//for (int i=0; i<k; i++) {
	//	printf("%g %g %g\n", gsl_vector_get(tpath,i), ));
	//}


	gsl_vector_free(y);
	gsl_vector_free(tpath);
	gsl_matrix_free(ypath);
	return 0;
}
