#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>


void print_matrix(gsl_matrix *M) {
	for(int row=0; row<M->size1; row++){
		for(int col=0; col<M->size2; col++) {
			printf("%.2f ", gsl_matrix_get(M, row, col));
		}
		printf("\n");
	}
}

void print_vector(gsl_vector *V) {
	for(int row=0; row<V->size; row++) {
		printf("%.2f\n", gsl_vector_get(V, row));
	}
}

void GS_QR_solve(gsl_matrix *Q, gsl_matrix *R, gsl_vector *b, gsl_vector *x) {
	int m = R->size1;
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
	for(int i=m-1; i>=0; i--) {
		double s = 0;
		for(int k=i+1; k<m; k++) {
			s += gsl_matrix_get(R, i, k) * gsl_vector_get(x, k);
		}
		gsl_vector_set(x, i, (gsl_vector_get(x, i)-s)/gsl_matrix_get(R, i, i));
	}
}

void GS_QR_decomp(gsl_matrix *A, gsl_matrix *R) {
	int m = A->size2;
	for(int i=0; i<m; i++) {
		gsl_vector_view Ai = gsl_matrix_column(A, i);
		double norm = gsl_blas_dnrm2(&Ai.vector);
		gsl_vector_scale(&Ai.vector, 1.0/norm);
		gsl_matrix_set(R, i, i, norm);

		for(int j=i+1; j<m; j++) {
			gsl_vector_view Aj = gsl_matrix_column(A, j);
			double dotprod = 0;
			gsl_blas_ddot(&Ai.vector, &Aj.vector, &dotprod);
			gsl_blas_daxpy(-dotprod, &Ai.vector, &Aj.vector);
			gsl_matrix_set(R, i, j, dotprod);
		}
	}
}

void GS_inverse(gsl_matrix *Q, gsl_matrix *R, gsl_matrix *B) {
	int n = R->size1;
	gsl_vector *x = gsl_vector_calloc(n);
	gsl_vector *b = gsl_vector_calloc(n);

	for(int i=0; i<n; i++) {
		gsl_vector_set(b, i, 1.0);
		GS_QR_solve(Q, R, b, x);
		gsl_vector_set(b, i, 0.0);
		gsl_matrix_set_col(B, i, x);
	}
}

double fitFunctions(int k, double x) {
	switch(k) {
		case 0:
			return 1.0/x;
			break;
		case 1:
			return 1.0;
			break;
		case 2:
			return x;
			break;
		default: {
			fprintf(stderr, "fitFunctions: k index out of bounds");
			return NAN;
		}
	}
}

void least_squares(gsl_vector *x, gsl_vector *y, gsl_vector *dy, int nf, gsl_vector *c, gsl_vector *xx, gsl_matrix *S) {
	int n = x->size, m = nf;

	gsl_matrix *A = gsl_matrix_alloc(n, m);
	gsl_matrix *R = gsl_matrix_alloc(m, m);
	gsl_matrix *I = gsl_matrix_alloc(m, m);
	gsl_matrix *Rinv = gsl_matrix_alloc(m, m);


	gsl_matrix_set_identity(I);

	for(int i=0; i<n; i++) {
		double xi = gsl_vector_get(x, i);
		double yi = gsl_vector_get(y, i);
		double dyi = gsl_vector_get(dy, i);
		gsl_vector_set(c, i, yi/dyi);
		for(int k=0; k<m; k++) {
			double func = fitFunctions(k, xi);
			gsl_matrix_set(A, i, k, func/dyi);
		}	
	}

	GS_QR_decomp(A, R);
	GS_QR_solve(A, R, c, xx);

	GS_inverse(I, R, Rinv);

	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, Rinv, Rinv, 0.0, S);


	gsl_matrix_free(A);
	gsl_matrix_free(I);
	gsl_matrix_free(R);
	gsl_matrix_free(Rinv);
}

int main() {
	int n = 10;
	int m = 3;

	gsl_vector *x = gsl_vector_alloc(n);
	gsl_vector *y = gsl_vector_alloc(n);
	gsl_vector *dy = gsl_vector_alloc(n);

	// Load data into vectors
	FILE *vecx = fopen("vectorx.txt", "r");
	FILE *vecy = fopen("vectory.txt", "r");
	FILE *vecdy = fopen("vectordy.txt", "r");
	gsl_vector_fscanf(vecx, x);
	gsl_vector_fscanf(vecy, y);
	gsl_vector_fscanf(vecdy, dy);
	fclose(vecx);
	fclose(vecy);
	fclose(vecdy);

	// Print data vectors
	printf("#x\t y\t dy\n");
	for(int i=0; i<n; i++) {
		double xi = gsl_vector_get(x, i);
		double yi = gsl_vector_get(y, i);
		double dyi = gsl_vector_get(dy, i);
		printf("%g\t %g\t %g\n", xi, yi, dyi);
	}
	printf("\n\n");


	// Make least squares fit
	double xmin = 0.09, xmax = 3, dx = 0.03, dotprod;
	gsl_vector *c = gsl_vector_alloc(n);
	gsl_vector *xx = gsl_vector_alloc(m);
	gsl_vector *a = gsl_vector_alloc(m);
	gsl_vector *Sa = gsl_vector_alloc(m);
	gsl_matrix *S = gsl_matrix_alloc(m, m);

	least_squares(x, y, dy, m, c, xx, S);

	printf("#x\t f\t f+df\t f-df\n");
	for(double k=xmin; k<xmax; k+=dx) {
		double f = 0, df;
		for(int i=0; i<m; i++) {
			gsl_vector_set(a, i, fitFunctions(i, k));
			f += gsl_vector_get(xx, i) * fitFunctions(i, k);
		}
	
		gsl_blas_dgemv(CblasNoTrans, 1.0, S, a, 0.0, Sa);
		gsl_blas_ddot(a, Sa, &dotprod);
		df = sqrt(dotprod);
		// Print fit
		printf("%g\t %g\t %g\t %g\n", k, f, f+df, f-df);
	}
}


