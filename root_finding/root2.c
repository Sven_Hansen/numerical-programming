#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>


void givens_QR_decomp(gsl_matrix* A) {
	for(int q=0; q<A->size2; q++) {
		for(int p=q+1; p<A->size1; p++) {
			double theta = atan2(gsl_matrix_get(A,p,q), gsl_matrix_get(A,q,q));
			for(int k=q; k<A->size2; k++) {
				double xq = gsl_matrix_get(A, q, k);
				double xp = gsl_matrix_get(A, p, k);
				gsl_matrix_set(A, q, k, xq*cos(theta) + xp*sin(theta));
				gsl_matrix_set(A, p, k, -xq*sin(theta) + xp*cos(theta));
			}
			gsl_matrix_set(A, p, q, theta);
		}
	}
}

void givens_QR_QTvec(gsl_matrix *QR, gsl_vector *v) {
	for(int q=0; q<QR->size2; q++) {
		for(int p=q+1; p<QR->size1; p++) {
			double theta = gsl_matrix_get(QR, p, q);
			double vq = gsl_vector_get(v, q);
			double vp = gsl_vector_get(v, p);
			gsl_vector_set(v, q, vq*cos(theta) + vp*sin(theta));
			gsl_vector_set(v, p, -vq*sin(theta) + vp*cos(theta));
		}
	}
}

void givens_QR_solve(gsl_matrix *QR, gsl_vector *b, gsl_vector *x) {
	givens_QR_QTvec(QR, b);

	for(int i=QR->size2-1; i>=0; i--) {
		double s = 0;
		for(int k=i+1; k<QR->size2; k++) {
			s += gsl_matrix_get(QR, i, k)*gsl_vector_get(x, k);
		}
		gsl_vector_set(x, i, (gsl_vector_get(b, i)-s)/gsl_matrix_get(QR, i, i));
	}
}

void GS_QR_decomp(gsl_matrix *A, gsl_matrix *R) {
	int m = A->size2;
	for(int i=0; i<m; i++) {
		gsl_vector_view Ai = gsl_matrix_column(A, i);
		double norm = gsl_blas_dnrm2(&Ai.vector);
		gsl_vector_scale(&Ai.vector, 1.0/norm);
		gsl_matrix_set(R, i, i, norm);

		for(int j=i+1; j<m; j++) {
			gsl_vector_view Aj = gsl_matrix_column(A, j);
			double dotprod = 0;
			gsl_blas_ddot(&Ai.vector, &Aj.vector, &dotprod);
			gsl_blas_daxpy(-dotprod, &Ai.vector, &Aj.vector);
			gsl_matrix_set(R, i, j, dotprod);
		}
	}
}

void GS_QR_solve(gsl_matrix *Q, gsl_matrix *R, gsl_vector *b, gsl_vector *x) {
	int m = R->size1;
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
	for(int i=m-1; i>=0; i--) {
		double s = 0;
		for(int k=i+1; k<m; k++) {
			s += gsl_matrix_get(R, i, k) * gsl_vector_get(x, k);
		}
		gsl_vector_set(x, i, (gsl_vector_get(x, i)-s)/gsl_matrix_get(R, i, i));
	}
}

int newton (void f(gsl_vector *x, gsl_vector *fx), gsl_vector *x, double dx, double eps) {

	int n = x->size;
	int ncalls = 0;

	gsl_vector *fx = gsl_vector_alloc(n); 
	gsl_vector *df = gsl_vector_alloc(n);
	gsl_vector *Dx = gsl_vector_alloc(n);
	gsl_vector *z = gsl_vector_alloc(n);
	gsl_vector *fz = gsl_vector_alloc(n);
	gsl_matrix *J = gsl_matrix_alloc(n,n);
	gsl_matrix *R = gsl_matrix_alloc(n,n); 

	while(1) {
		f(x,fx);
		for (int j=0; j<n; j++) {
			gsl_vector_set(x,j,gsl_vector_get(x,j)+dx);
			f(x,df);
			gsl_vector_sub(df,fx);
			for (int i=0; i<n; i++) {
				gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx);
			}
			gsl_vector_set(x,j,gsl_vector_get(x,j)-dx);
		}
		GS_QR_decomp(J,R);
		GS_QR_solve(J,R,fx,Dx);
		gsl_vector_scale(Dx,-1);
		double s=1;

		while(1) {
			ncalls++;
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,Dx);
			f(z,fz);
			if (gsl_blas_dnrm2(fz)<(1-s/2)*gsl_blas_dnrm2(fx) || s<0.02) {
				break;
			}
			s*=0.5;
			gsl_vector_scale(Dx,0.5);
		}
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(fx,fz);
		if (gsl_blas_dnrm2(Dx)<dx || gsl_blas_dnrm2(fx)<eps) {
			break;
		}
	}

	gsl_vector_free(fx);
	gsl_vector_free(df);
	gsl_vector_free(Dx);
	gsl_vector_free(z);
	gsl_vector_free(fz);
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	return ncalls;
}




void print_vector(gsl_vector *V) {
	for(int row=0; row<V->size; row++) {
		printf("%.2f\n", gsl_vector_get(V, row));
	}
}


int newton_jacobi(void f(gsl_vector *x, gsl_vector *fx), void jacobian(gsl_vector *x, gsl_matrix *J), gsl_vector *x, double dx, double eps) {
	int n = x->size;
	int ncalls = 0;

	gsl_matrix *J = gsl_matrix_alloc(n,n);
	gsl_vector *z = gsl_vector_alloc(n);
	gsl_vector *fz = gsl_vector_alloc(n);
	gsl_vector *fx = gsl_vector_alloc(n);
	gsl_vector *df = gsl_vector_alloc(n);
	gsl_vector *Dx = gsl_vector_alloc(n);

	do {
		ncalls++;
		f(x,fx);
		jacobian(x,J);
		givens_QR_decomp(J);
		gsl_vector_scale(fx,-1);
		givens_QR_solve(J,fx,Dx);
		gsl_vector_scale(fx,-1);
		double s = 2;
		do{
			s/=2;
			gsl_vector_memcpy(z,Dx);
			gsl_vector_scale(z,s);
			gsl_vector_add(z,x);
			f(z,fz);
		}while (gsl_blas_dnrm2(fz)>(1-s/2)*gsl_blas_dnrm2(fx) && s>0.02);
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(fx,fz);
	}while (gsl_blas_dnrm2(Dx)>dx && gsl_blas_dnrm2(fx)>eps);

	gsl_matrix_free(J);
	gsl_vector_free(z);
	gsl_vector_free(fz);
	gsl_vector_free(fx);
	gsl_vector_free(df);
	gsl_vector_free(Dx);
	return ncalls;
}

void A (gsl_vector *p, gsl_vector *fx) {
	double A = 10000;
	double x = gsl_vector_get(p,0);
	double y = gsl_vector_get(p,1);
	double f_x = A*x*y - 1;
	double f_y = exp(-1*x) + exp(-1*y) - 1 - 1./A;;
	gsl_vector_set(fx,0,f_x);
	gsl_vector_set(fx,1,f_y);
}

void A_J (gsl_vector *p, gsl_matrix *J) {
	double A = 10000;
	double x = gsl_vector_get(p,0);
	double y = gsl_vector_get(p,1);
	double J00, J11, J01, J10;
	J00 = A*y;
	J01 = A*x;
	J10 = -1*exp(-1*x);
	J11 = -1*exp(-1*y);
	gsl_matrix_set(J,0,0,J00);
	gsl_matrix_set(J,0,1,J01);
	gsl_matrix_set(J,1,0,J10);
	gsl_matrix_set(J,1,1,J11);
}

void rosenbrock (gsl_vector *p, gsl_vector *fx) {
	double x = gsl_vector_get(p,0);
	double y = gsl_vector_get(p,1);
	double f_x = 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x;
	double f_y = 100*2*(y-x*x);
	gsl_vector_set(fx,0,f_x);
	gsl_vector_set(fx,1,f_y);
}

void rosenbrock_J (gsl_vector *p, gsl_matrix *J) {
	double x = gsl_vector_get(p,0);
	double y = gsl_vector_get(p,1);
	double J00, J11, J01, J10;
	J00 = 2-400*(y-3*pow(x,2));
	J01 = -400*x;
	J10 = -400*x;
	J11 = 200;
	gsl_matrix_set(J,0,0,J00);
	gsl_matrix_set(J,0,1,J01);
	gsl_matrix_set(J,1,0,J10);
	gsl_matrix_set(J,1,1,J11);
}

void himmelblau (gsl_vector *p, gsl_vector *fx) {
	double x = gsl_vector_get(p,0);
	double y = gsl_vector_get(p,1);
	double f_x = 4*x*(pow(x,2)+y-11) + 2*(x+pow(y,2)-7);
	double f_y = 2*(pow(x,2)+y-11) + 4*y*(x+pow(y,2)-7);
	gsl_vector_set(fx,0,f_x);
	gsl_vector_set(fx,1,f_y);
}

void himmelblau_J (gsl_vector *p, gsl_matrix *J) {
	double x = gsl_vector_get(p,0);
	double y = gsl_vector_get(p,1);
	double J00, J11, J01, J10;
	J00 = 12*pow(x,2) + 4*y - 42;
	J01 = 4*(x+y);
	J10 = 4*(x+y);
	J11 = 12*pow(y,2) + 4*x - 26;
	gsl_matrix_set(J,0,0,J00);
	gsl_matrix_set(J,0,1,J01);
	gsl_matrix_set(J,1,0,J10);
	gsl_matrix_set(J,1,1,J11);
}

int main () {
	gsl_vector *fx = gsl_vector_alloc(2);
	gsl_vector *x = gsl_vector_alloc(2);
	int calls = 0;

	//Part A
	//A
	calls = 0;
	gsl_vector_set(x,0,1);
	gsl_vector_set(x,1,10);

	printf("PART A - Newton \n\n");
	printf("Initial vector x\n");
	print_vector(x);
	
	calls = newton(A,x,1e-6,1e-3);
	printf("\nRoots for A:\n");
	print_vector(x);
	printf("Number of calls: %d\n", calls);
	//Rosenbrock
	gsl_vector_set(x,0,1);
	gsl_vector_set(x,1,10);

	calls = newton(rosenbrock,x,1e-6,1e-3);
	printf("\nRoots for Rosenbrock:\n");
	print_vector(x);
	printf("Number of calls: %d\n", calls);
	newton(rosenbrock,x,1e-6,1e-3);
	//Himmelblau
	gsl_vector_set(x,0,1);
	gsl_vector_set(x,1,10);

	calls = newton(himmelblau,x,1e-6,1e-3);
	printf("\nRoots for Himmelblau:\n");
	print_vector(x);
	printf("Number of calls: %d\n", calls);
	newton(himmelblau,x,1e-6,1e-3);


	//Part B
	//A
	printf("\n\nPART B - Newton with Jacobian\n\n");
	gsl_vector_set(x,0,1);
	gsl_vector_set(x,1,10);
	printf("Initial vector x\n");
	print_vector(x);
	
	calls = newton_jacobi(A,A_J,x,1e-6,1e-3);
	printf("\nRoots for A:\n");
	print_vector(x);
	printf("Number of calls: %d\n", calls);
	//Rosenbrock
	gsl_vector_set(x,0,1);
	gsl_vector_set(x,1,10);
	printf("Initial vector x\n");
	print_vector(x);
	
	calls = newton_jacobi(rosenbrock,rosenbrock_J,x,1e-6,1e-3);
	printf("\nRoots for Rosenbrock:\n");
	print_vector(x);
	printf("Number of calls: %d\n", calls);
	//Himmelblau
	gsl_vector_set(x,0,1);
	gsl_vector_set(x,1,10);
	printf("Initial vector x\n");
	print_vector(x);
	
	calls = newton_jacobi(himmelblau,himmelblau_J,x,1e-6,1e-3);
	printf("\nRoots for Himmelblau:\n");
	print_vector(x);
	printf("Number of calls: %d\n", calls);

	gsl_vector_free(fx);
	gsl_vector_free(x);
	return 0;
}
