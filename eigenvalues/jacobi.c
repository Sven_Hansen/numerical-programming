#include<stdio.h>
#include<math.h>
#include<time.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

#define RND (double)rand()/RAND_MAX


int jacobi_cyclic(gsl_matrix *A, gsl_vector *e, gsl_matrix *V) {
	int changed, sweeps = 0, n = A->size1;
	for(int i=0; i<n; i++) {
		gsl_vector_set(e, i, gsl_matrix_get(A, i, i));
	}
	gsl_matrix_set_identity(V);

	do{
		changed = 0;
		sweeps++;
		int p, q;
		for(p=0; p<n; p++) {
			for(q=p+1; q<n; q++) {
				double app = gsl_vector_get(e, p);
				double aqq = gsl_vector_get(e, q);
				double apq = gsl_matrix_get(A, p, q);
				double phi = 0.5 * atan2(2*apq, aqq-app);
				double c = cos(phi), s = sin(phi);
				double app1 = c*c*app-2*s*c*apq+s*s*aqq;
				double aqq1 = s*s*app+2*s*c*apq+c*c*aqq;

				if(app1!=app || aqq1!=aqq) {
					changed = 1;
					gsl_vector_set(e, p, app1);
					gsl_vector_set(e, q, aqq1);
					gsl_matrix_set(A, p, q, 0.0);
					for(int i=0; i<p; i++) {
						double aip = gsl_matrix_get(A, i, p);
						double aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, i, p, c*aip-s*aiq);
						gsl_matrix_set(A, i, q, c*aiq+s*aip);
					}
					for(int i=p+1; i<q; i++) {
						double api = gsl_matrix_get(A, p, i);
						double aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, p, i, c*api-s*aiq);
						gsl_matrix_set(A, i, q, c*aiq+s*api);
					}
					for(int i=q+1; i<n; i++) {
						double api = gsl_matrix_get(A, p, i);
						double aqi = gsl_matrix_get(A, q, i);
						gsl_matrix_set(A, p, i, c*api-s*aqi);
						gsl_matrix_set(A, q, i, c*aqi+s*api);
					}
					for(int i=0; i<n; i++) {
						double vip = gsl_matrix_get(V, i, p);
						double viq = gsl_matrix_get(V, i, q);
						gsl_matrix_set(V, i, p, c*vip-s*viq);
						gsl_matrix_set(V, i, q, c*viq+s*vip);
					}
				}
			}
		}
	}while(changed!=0);
	return sweeps;
}

int jacobi_min(gsl_matrix *A, gsl_vector *e, gsl_matrix *V, int N) {
	int changed, p, q, sweeps = 0, n = A->size1;
	for(int i=0; i<n; i++) {
		gsl_vector_set(e, i, gsl_matrix_get(A, i, i));
	}
	gsl_matrix_set_identity(V);
	for(p=0; p<N; p++) {
		do{
			changed = 0;
			sweeps++;
		
			for(q=p+1; q<n; q++) {
				double app = gsl_vector_get(e, p);
				double aqq = gsl_vector_get(e, q);
				double apq = gsl_matrix_get(A, p, q);
				double phi = 0.5 * atan2(2*apq, aqq-app);
				double c = cos(phi), s = sin(phi);
				double app1 = c*c*app-2*s*c*apq+s*s*aqq;
				double aqq1 = s*s*app+2*s*c*apq+c*c*aqq;

				if(app1!=app || aqq1!=aqq) {
					changed = 1;
					gsl_vector_set(e, p, app1);
					gsl_vector_set(e, q, aqq1);
					gsl_matrix_set(A, p, q, 0.0);
					for(int i=0; i<p; i++) {
						double aip = gsl_matrix_get(A, i, p);
						double aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, i, p, c*aip-s*aiq);
						gsl_matrix_set(A, i, q, c*aiq+s*aip);
					}
					for(int i=p+1; i<q; i++) {
						double api = gsl_matrix_get(A, p, i);
						double aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, p, i, c*api-s*aiq);
						gsl_matrix_set(A, i, q, c*aiq+s*api);
					}
					for(int i=q+1; i<n; i++) {
						double api = gsl_matrix_get(A, p, i);
						double aqi = gsl_matrix_get(A, q, i);
						gsl_matrix_set(A, p, i, c*api-s*aqi);
						gsl_matrix_set(A, q, i, c*aqi+s*api);
					}
					for(int i=0; i<n; i++) {
						double vip = gsl_matrix_get(V, i, p);
						double viq = gsl_matrix_get(V, i, q);
						gsl_matrix_set(V, i, p, c*vip-s*viq);
						gsl_matrix_set(V, i, q, c*viq+s*vip);
					}
				}
			}
		}while(changed!=0);
	}
	return sweeps;
}

int jacobi_max(gsl_matrix *A, gsl_vector *e, gsl_matrix *V, int N) {
	int changed, p, q, sweeps = 0, n = A->size1;
	for(int i=0; i<n; i++) {
		gsl_vector_set(e, i, gsl_matrix_get(A, i, i));
	}
	gsl_matrix_set_identity(V);
	for(p=0; p<N; p++) {
		do{
			changed = 0;
			sweeps++;
		
			for(q=p+1; q<n; q++) {
				double app = gsl_vector_get(e, p);
				double aqq = gsl_vector_get(e, q);
				double apq = gsl_matrix_get(A, p, q);
				double phi = 0.5 * atan2(2*apq, aqq-app);
				double c = cos(phi+M_PI/2), s = sin(phi+M_PI/2);
				double app1 = c*c*app-2*s*c*apq+s*s*aqq;
				double aqq1 = s*s*app+2*s*c*apq+c*c*aqq;

				if(app1!=app || aqq1!=aqq) {
					changed = 1;
					gsl_vector_set(e, p, app1);
					gsl_vector_set(e, q, aqq1);
					gsl_matrix_set(A, p, q, 0.0);
					for(int i=0; i<p; i++) {
						double aip = gsl_matrix_get(A, i, p);
						double aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, i, p, c*aip-s*aiq);
						gsl_matrix_set(A, i, q, c*aiq+s*aip);
					}
					for(int i=p+1; i<q; i++) {
						double api = gsl_matrix_get(A, p, i);
						double aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, p, i, c*api-s*aiq);
						gsl_matrix_set(A, i, q, c*aiq+s*api);
					}
					for(int i=q+1; i<n; i++) {
						double api = gsl_matrix_get(A, p, i);
						double aqi = gsl_matrix_get(A, q, i);
						gsl_matrix_set(A, p, i, c*api-s*aqi);
						gsl_matrix_set(A, q, i, c*aqi+s*api);
					}
					for(int i=0; i<n; i++) {
						double vip = gsl_matrix_get(V, i, p);
						double viq = gsl_matrix_get(V, i, q);
						gsl_matrix_set(V, i, p, c*vip-s*viq);
						gsl_matrix_set(V, i, q, c*viq+s*vip);
					}
				}
			}
		}while(changed!=0);
	}
	return sweeps;
}
void print_matrix(gsl_matrix *M) {
	for(int row=0; row<M->size1; row++){
		for(int col=0; col<M->size2; col++) {
			printf("%.2f\t", gsl_matrix_get(M, row, col));
		}
		printf("\n");
	}
}

void print_vector(gsl_vector *V) {
	for(int row=0; row<V->size; row++) {
		printf("%.2f\n", gsl_vector_get(V, row));
	}
}


int main() {
	time_t t;
	srand((unsigned) time(&t));
	int dim = 6;

	gsl_matrix *A = gsl_matrix_alloc(dim,dim);
	gsl_matrix *AA = gsl_matrix_alloc(dim,dim);
	gsl_matrix *AAA = gsl_matrix_alloc(dim,dim);
	gsl_matrix *AOld = gsl_matrix_alloc(dim,dim);
	gsl_matrix *V = gsl_matrix_alloc(dim,dim);
	gsl_matrix *VV = gsl_matrix_alloc(dim,dim);
	gsl_matrix *VVV = gsl_matrix_alloc(dim,dim);
	gsl_matrix *VTA = gsl_matrix_alloc(dim,dim);
	gsl_matrix *VTAV = gsl_matrix_alloc(dim,dim);
	gsl_vector *e = gsl_vector_alloc(dim);
	gsl_vector *ee = gsl_vector_alloc(dim);
	gsl_vector *eee = gsl_vector_alloc(dim);
	for(int i=0; i<dim; i++) {
		for(int j=0; j<dim; j++) {
			double randNumber = RND;
			gsl_matrix_set(A, i, j, randNumber);
			gsl_matrix_set(A, j, i, randNumber);
			gsl_matrix_set(AA, i, j, randNumber);
			gsl_matrix_set(AA, j, i, randNumber);
			gsl_matrix_set(AAA, i, j, randNumber);
			gsl_matrix_set(AAA, j, i, randNumber);
		}
	}
	
	//Test A
	printf("PART A\n");
	printf("Matrix A\n");
	print_matrix(A);
	gsl_matrix_memcpy(AOld, A);

	int sweeps = jacobi_cyclic(A, e, V);
	
	printf("\nTest that A is now lower triangular:");
	printf("\nMatrix A\n");
	print_matrix(A);
	
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, V, AA, 0.0, VTA);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, VTA, V, 0.0, VTAV);
	printf("\nTest that V^TAV is diagonal:");
	printf("\nMatrix V^TAV\n");
	print_matrix(VTAV);

	printf("\nTest that vector e contains the diagonal of V^TAV:");
	printf("\nVector e\n");
	print_vector(e);


	//Test B
	printf("\nPART B");
	int evalues = dim;
	int sweeps2 = jacobi_min(AA, ee, VV, evalues);

	printf("\nTest that e contains the %d first eigenvalues of A (min first):", evalues);
	printf("\nVector e\n");
	print_vector(ee);

	int sweeps3 = jacobi_max(AAA, eee, VVV, evalues);

	printf("\nTest that e contains the %d first eigenvalues of A (max first):", evalues);
	printf("\nVector e\n");
	print_vector(eee);

	printf("\nNumber of sweeps for cyclic: %d", sweeps);
	printf("\nNumber of sweeps for value-by-value (min): %d", sweeps2);
	printf("\nNumber of sweeps for value-by-value (max): %d", sweeps3);
	
	
	
	gsl_matrix_free(A);
	gsl_matrix_free(AA);
	gsl_matrix_free(AAA);
	gsl_matrix_free(AOld);
	gsl_matrix_free(V);
	gsl_matrix_free(VTA);
	gsl_matrix_free(VTAV);
	gsl_vector_free(e);
	gsl_vector_free(ee);
	gsl_vector_free(eee);
	return 0;
}
