Numerical Methods 2017 exam project.
Sven Hansen, 201405109.

Assignment 14:
Advanced step-size control for ODE.
Implement an ODE driver that accepts the step if the following set of conditions is satisfied
dy_k < tol_k = (acc + eps*|y_k|) * sqrt(h/(b-a))
for all k = 1,...,n where n is the number components in the vector function y.


The solution for part B in the ODE exercises is used as a basis and the driver is updated to fulfil the above qualities.
