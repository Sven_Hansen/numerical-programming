#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>


void rkstep12 (double t, double h, gsl_vector *y, void f(double t, gsl_vector *y, gsl_vector *dydt), gsl_vector *yh, gsl_vector *err) {
	int n = y->size;
	
	gsl_vector *k0 = gsl_vector_alloc(n);
	gsl_vector *k12 = gsl_vector_alloc(n);
	gsl_vector *yt = gsl_vector_alloc(n);

	f(t,y,k0);
	for (int i=0; i<n; i++) {
		gsl_vector_set(yt,i,gsl_vector_get(y,i) + h/2*gsl_vector_get(k0,i));
	}
	f(t+h/2,yt,k12);
	for (int i=0; i<n; i++) {
		gsl_vector_set(yh,i,gsl_vector_get(y,i) + h*gsl_vector_get(k12,i));
		gsl_vector_set(err,i,(gsl_vector_get(k0,i) - gsl_vector_get(k12,i))*h/2);
	}


	gsl_vector_free(k0);
	gsl_vector_free(k12);
	gsl_vector_free(yt);
}

//Driver with path from ODE exercises with added advanced step-size control
int driver (gsl_vector *tpath, gsl_matrix *ypath, double end, double h, gsl_vector *y, double acc, double eps, int max, void stepper(double t, double h, gsl_vector *y, void f(double t, gsl_vector *y, gsl_vector *dydt), gsl_vector *yh, gsl_vector *err), void f(double t, gsl_vector *y, gsl_vector *dydt)) {
	int n = y->size;
	int k = 0, condition;
	double start = gsl_vector_get(tpath,0);
	
	gsl_vector *yh = gsl_vector_alloc(n);
	gsl_vector *dy = gsl_vector_alloc(n);
	gsl_vector *tolerr = gsl_vector_alloc(n);

	while(gsl_vector_get(tpath,k)<end) {
		//Reset acceptance condition.
		condition = 1;
		double t = gsl_vector_get(tpath,k);
		if (t+h>end) {
			h = end-t;
		}
		stepper(t,h,y,f,yh,dy);

		//Calculate the local error and tolerance. If a local error is larger than the corresponding local tolerance set the acceptance condition to 0 to reject the step.
		for (int i=0; i<n; i++) {
			double erri = fabs(gsl_vector_get(dy,i));
			double toli = (eps*fabs(gsl_vector_get(yh,i))+acc)*sqrt(h/(end-start));
			double tolerri = toli/erri;
			gsl_vector_set(tolerr,i,tolerri);
			if (erri > toli) {
				condition = 0;
			}
		}
		//Find the smallest tolerance to error ratio for use in step-size calculation.
		double min = gsl_vector_min(tolerr);

		if (condition==1) {
			k++;
			if (k>max-1) {
				return -k;
			}
			gsl_vector_set(tpath,k,t+h);
			for (int i=0; i<n; i++) {
				gsl_vector_set(y,i,gsl_vector_get(yh,i));
				gsl_matrix_set(ypath,k,i,gsl_vector_get(y,i));
			}
		}
		//Step-size calculation.
		h *= pow(min,0.25)*0.95;
	}
	
	gsl_vector_free(yh);
	gsl_vector_free(dy);
	gsl_vector_free(tolerr);
	return k;
}

//Sine function for tests
void sine (double t, gsl_vector *y, gsl_vector *dydt) {
	gsl_vector_set(dydt,0,gsl_vector_get(y,1));
	gsl_vector_set(dydt,1,-1*gsl_vector_get(y,0));
}

int main () {
	int dim = 2, max = 1e7;
	double t =0, b = 2*M_PI, h = 0.1, acc = 1e-2, eps = 1e-2;

	gsl_vector *tpath = gsl_vector_alloc(max);
	gsl_matrix *ypath = gsl_matrix_alloc(max,dim);
	gsl_vector *y = gsl_vector_alloc(dim);
	gsl_vector_set(y,0,0);
	gsl_vector_set(y,1,1);

	printf("Startpoint\ta = %g\nEndpoint\tb = %g\n", t, b);
	int k = driver(tpath,ypath,b,h,y,acc,eps,max,rkstep12,sine);
	if (k<0) {
		printf("Maximum step count reached\n");
	}
	printf("Function\ty(b) = %g\nDifferential\ty'(b) = %g\n", gsl_vector_get(y,0), gsl_vector_get(y,1));
	printf("Number of accepted steps: %d\n", k);

	printf("\n\n");
	for (int i=0; i<k; i++) {
		printf("%g %g %g %g %g\n", gsl_vector_get(tpath,i), gsl_matrix_get(ypath,i,0), gsl_matrix_get(ypath,i,1), sin(gsl_vector_get(tpath,i)), cos(gsl_vector_get(tpath,i)));
	}

	gsl_vector_free(y);
	return 0;
}
